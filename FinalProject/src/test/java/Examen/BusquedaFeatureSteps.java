package Examen;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;

public class BusquedaFeatureSteps {

    Configuracion miConfig = new Configuracion();
    BusquedaSelectores busquedaSelectores = new BusquedaSelectores();

    //Metodo para acceder a la pagina
    @Given("Acceder a la pagina {string}")
    public void acceder_a_la_pagina(String url) {
        miConfig.accederPagina(url);
    }

    //Metodos para el escenario busqueda exitosa y busqueda invalida
    @Given("Seleccionar barra de busqueda {string}")
    public void seleccionarBarraDeBusqueda(String arg0) {
        miConfig.clickElement(BusquedaSelectores.barraBusqueda);
    }
    
    @When("Escribir producto {string}")
    public void escribirProducto(String producto) {
        miConfig.escribir(BusquedaSelectores.barraBusqueda, producto);
    }

    //Metodo para validar si existe el producto, negando que "el elemento no esta presente"
    @Then("Validar producto {string}")
    public void validarProducto(String producto) {
        boolean productoEncontrado = !miConfig.isElementNotPresent(By.xpath(BusquedaSelectores.appleProducto));
        if (productoEncontrado) {
            System.out.println("El producto '" + producto + "' se ha encontrado correctamente.");
        } else {
            System.out.println("No se ha encontrado el producto '" + producto + "'.");
        }
    }

    /*Metodo para validar si existe el mensaje, negando que "el elemento no esta presente"
    para el escenario de busqueda invalida */
    @Then("Validar mensaje {string}")
    public void validarMensaje(String mensaje) {
        boolean mensajePresente = !miConfig.isElementNotPresent(By.xpath(BusquedaSelectores.mensajeNoProducto));
        if (mensajePresente) {
            System.out.println("El mensaje '" + mensaje + "' se ha mostrado correctamente.");
        } else {
            System.out.println("No se ha mostrado el mensaje '" + mensaje + "'.");
        }
    }
}
