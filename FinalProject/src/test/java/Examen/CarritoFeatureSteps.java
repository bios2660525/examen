package Examen;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CarritoFeatureSteps {

    Configuracion miConfig = new Configuracion();
    CarritoSelectores carritoSelectores = new CarritoSelectores();

    //Metodo para acceder a la pagina
    @Given("Acceder a la pag {string}")
    public void accederALaPag(String url) {
        miConfig.accederPagina(url);
    }


    @Given("Clickear el boton carrito {string}")
    public void clickearBotonCarrito(String elemento) {
        miConfig.clickElement(CarritoSelectores.carrito);
    }

    //Metodo para validar escenario de carrito vacío
    @Then("Validar el mensaje {string}")
    public void validarMensajeCarritoVacio(String mensaje) {
        boolean mensajePresente = !miConfig.isElementNotPresent(By.xpath(CarritoSelectores.mensajeCarritoVacio));
        if (mensajePresente) {
            System.out.println("El mensaje '" + mensaje + "' se ha mostrado correctamente.");
        } else {
            System.out.println("No se ha mostrado el mensaje '" + mensaje + "'.");
        }
    }

    // Método para agregar el producto indicado al carrito
    @Given("Clickear agregar producto {string}")
    public void clickearAgregarProducto(String producto) {
        switch (producto.toLowerCase()) {
            case "brocoli":
                miConfig.clickElement(CarritoSelectores.addToCartBrocoli);
                break;
            case "zanahoria":
                miConfig.clickElement(CarritoSelectores.addToCartZanahoria);
                break;
            case "tomate":
                miConfig.clickElement(CarritoSelectores.addToCartTomate);
                break;
            default:
                throw new IllegalArgumentException("Producto no reconocido: " + producto);
        }
    }

    //Metodo para validar que el producto se agrego al carrito
    @Then("Validar existencia producto {string}")
    public void validarExistenciaProducto(String producto) {
        String productoXpath = "";
        if (producto.equalsIgnoreCase("Brocoli")) {
            productoXpath = CarritoSelectores.brocoliCarrito;
        } else {
            throw new IllegalArgumentException("Producto no reconocido: " + producto);
        }

        boolean productoPresente = !miConfig.isElementNotPresent(By.xpath(productoXpath));
        if (!productoPresente) {
            throw new AssertionError("El producto '" + producto + "' no está presente en el carrito.");
        }
    }

    //Metodo para escenario de eliminar producto del carrito
    @And("Clickear boton eliminar {string}")
    public void clickearBotonEliminar(String eliminar) {
        if (eliminar.equalsIgnoreCase("Eliminar")) {
            miConfig.clickElement(CarritoSelectores.botonEliminar);
        } else {
            throw new IllegalArgumentException("Botón no reconocido: " + eliminar);
        }
    }

    //Metodo para ingresar cantidad "0" de un producto al carrito
    @Given("Escribir cantidad de producto {string} {string}")
    public void escribirCantidadDeProducto(String producto, String cantidad) {
        String cantidadXpath = "";
        if (producto.equalsIgnoreCase("Tomate")) {
            cantidadXpath = CarritoSelectores.campoCantidadTomate;
        } else {
            throw new IllegalArgumentException("Producto no reconocido: " + producto);
        }

        miConfig.escribir(cantidadXpath, cantidad);
    }

    //Metodo para acceder a checkout
    @And("Clickear el boton proceder al checkout {string}")
    public void clickearBotonProceedCheckOut(String boton) {
        if (boton.equalsIgnoreCase("Checkout")) {
            miConfig.clickElement(CarritoSelectores.botonProceedCheckOut);
        } else {
            System.out.println("Botón no reconocido: " + boton);
        }
    }

    //Metodo para ingresar un codigo de descuento
    @When("Escribir codigo de promo {string}")
    public void escribirCodigoPromo(String codigo) {
        miConfig.escribir(CarritoSelectores.campoCodigoPromo, codigo);
    }

    //Metodo para aplicar codigo de descuento
    @And("Clickear el boton aplicar promo {string}")
    public void clickearBotonAplicarPromo(String boton) {
        if (boton.equalsIgnoreCase("Aplicar")) {
            miConfig.clickElement(CarritoSelectores.botonAplicar);
        } else {
            System.out.println("Botón no reconocido: " + boton);
        }
    }

    //Metodo para validar el mensaje que indica codigo de descuento invalido
    @Then("Validar el mensaje de promo {string}")
    public void validarMensajePromo(String mensajeEsperado) {
        String mensajeActual = miConfig.obtenerTexto(CarritoSelectores.mensajeCodigoInvalido);
        if (mensajeActual.contains(mensajeEsperado)) {
            System.out.println("Mensaje de promo validado correctamente: " + mensajeActual);
        } else {
            System.out.println("Error: El mensaje de promo esperado '" + mensajeEsperado + "' no coincide con el actual: " + mensajeActual);
        }
    }

    //Metodo para verificar que se alerta de un error en el carrito
    @Then("Validar el mensaje no se puede continuar {string}")
    public void validarMensajeNoSePuedeContinuar(String mensajeEsperado) {
        WebDriver driver = miConfig.getDriver();

        try {
            //Se busca el elemento que contiene el texto de error
            WebElement mensajeElemento = driver.findElement(By.xpath(CarritoSelectores.mensajeErrorCarrito));
            String mensajeActual = mensajeElemento.getText();

            //Se valida si el mensaje es igual al esperado
            assertEquals("El mensaje no coincide", mensajeEsperado, mensajeActual);
            System.out.println("El mensaje '" + mensajeEsperado + "' se ha mostrado correctamente.");
        } catch (org.openqa.selenium.NoSuchElementException e) {
            // Se alerta si no se encuentra el mensaje
            System.out.println("No se ha mostrado el mensaje '" + mensajeEsperado + "'.");
            throw new AssertionError("El mensaje no se ha mostrado correctamente: " + mensajeEsperado);
        }
    }
}
