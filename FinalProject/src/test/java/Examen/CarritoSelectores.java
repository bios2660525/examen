package Examen;

public class CarritoSelectores {

    static String carrito = "//div[@id='root']/div/header/div/div[3]/a[4]/img";

    static String mensajeCarritoVacio = "//div[@id='root']/div/header/div/div[3]/div[2]/div/div/div/h2";

    static String addToCartBrocoli = "//div[@id='root']/div/div/div/div/div[3]/button";

    static String brocoliCarrito = "//div[@id='root']/div/header/div/div[3]/div[2]/div/div/ul/li/div/p";

    static String addToCartZanahoria = "//div[@id='root']/div/div/div/div[5]/div[3]/button";

    static String botonEliminar = "//div[@id='root']/div/header/div/div[3]/div[2]/div/div/ul/li/a";

    static String botonProceedCheckOut = "//div[@id='root']/div/header/div/div[3]/div[2]/div[2]/button";

    static String campoCodigoPromo = "//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]";

    static String botonAplicar = "//button[contains(.,'Apply')]";

    static String mensajeCodigoInvalido = "//span[contains(.,'Invalid code ..!')]";

    static String campoCantidadTomate = "//div[@id='root']/div/div/div/div[6]/div[2]/input";

    static String addToCartTomate = "//div[@id='root']/div/div/div/div[6]/div[3]/button";

    static String mensajeErrorCarrito = "//div[@id='cart']//span[contains(text(),'Error: Check your cart')]";

}
