package Examen;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.junit.Assert.assertEquals;

public class CheckOutFeatureSteps {

    Configuracion miConfig = new Configuracion();
    CheckOutSelectores checkOutSelectores = new CheckOutSelectores();
    CarritoSelectores carritoSelectores = new CarritoSelectores();


    @Given("Ingresar a la pagina {string}")
    public void ingresarAPagina(String url) {
        miConfig.getDriver().get(url);
    }


    @Given("Clickear agregar un producto {string}")
    public void clickearAgregarProducto(String producto) {
        WebElement addButton;
        switch (producto) {
            case "brocoli":
                addButton = miConfig.getDriver().findElement(By.xpath(CarritoSelectores.addToCartBrocoli));
                break;
            case "tomate":
                addButton = miConfig.getDriver().findElement(By.xpath(CarritoSelectores.addToCartTomate));
                break;
            default:
                throw new IllegalArgumentException("Producto no encontrado: " + producto);
        }
        addButton.click();
    }


    @When("Ingresar al carrito {string}")
    public void ingresarAlCarrito(String carrito) {
        WebElement cartButton = miConfig.getDriver().findElement(By.xpath(CarritoSelectores.carrito));
        cartButton.click();
    }


    @And("Clickear boton checkout {string}")
    public void clickearBotonCheckout(String boton) {
        WebDriverWait wait = new WebDriverWait(miConfig.getDriver(), Duration.ofSeconds(10));
        WebElement checkoutButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CarritoSelectores.botonProceedCheckOut)));
        checkoutButton.click();
    }


    @And("Clickear boton de pedido {string}")
    public void clickearBotonDePedido(String boton) {
        WebDriverWait wait = new WebDriverWait(miConfig.getDriver(), Duration.ofSeconds(10));
        WebElement placeOrderButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckOutSelectores.botonPlaceOrder)));
        placeOrderButton.click();
    }


    @And("Clickear combobox {string}")
    public void clickearComboBox(String comboBox) {
        WebElement comboBoxElement = miConfig.getDriver().findElement(By.xpath(CheckOutSelectores.comboPaises));
        comboBoxElement.click();
    }


    @And("Seleccionar pais {string}")
    public void seleccionarPais(String pais) {
        WebDriverWait wait = new WebDriverWait(miConfig.getDriver(), Duration.ofSeconds(10));
        WebElement comboPaises = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckOutSelectores.comboPaises)));
        Select paisSelect = new Select(comboPaises);
        paisSelect.selectByVisibleText(pais);
    }

    @And("Clickear checkbox de condiciones {string}")
    public void clickearCheckboxCondiciones(String checkboxCondiciones) {
        WebDriverWait wait = new WebDriverWait(miConfig.getDriver(), Duration.ofSeconds(10));
        WebElement checkbox = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckOutSelectores.checkTermsConditions)));
        checkbox.click();
    }

    @And("Clickear boton para finalizar {string}")
    public void clickearBotonFinalizar(String boton) {
        WebDriverWait wait = new WebDriverWait(miConfig.getDriver(), Duration.ofSeconds(10));
        WebElement botonFinalizar = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(CheckOutSelectores.botonProceed)));
        botonFinalizar.click();
    }


    @Then("Validar mensaje de confirmacion {string}")
    public void validarMensajeConfirmacion(String mensajeEsperado) {
        WebDriverWait wait = new WebDriverWait(miConfig.getDriver(), Duration.ofSeconds(10));
        WebElement mensajeElemento = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CheckOutSelectores.mensajeCompraRealizada)));
        String mensajeActual = mensajeElemento.getText();

        mensajeEsperado = "Thank you, your order has been placed successfully\nYou'll be redirected to Home page shortly!!";
        assertEquals("El mensaje no coincide", mensajeEsperado, mensajeActual);
    }

    @Given("Escribir la cantidad de producto {string} {string}")
    public void escribirCantidadDeProducto(String producto, String cantidad) {
        WebElement cantidadInput;
        switch (producto) {
            case "tomate":
                cantidadInput = miConfig.getDriver().findElement(By.xpath(CarritoSelectores.campoCantidadTomate));
                break;
            default:
                throw new IllegalArgumentException("Producto no encontrado: " + producto);
        }
        cantidadInput.clear();
        cantidadInput.sendKeys(cantidad);
    }

    //Metodo para validar que no se permite comprar con cantidad 0 del producto
    @Then("Validar el mensaje de error {string}")
    public void validarMensajeDeError(String mensajeEsperado) {
        WebDriver driver = miConfig.getDriver();

        try {
            // Se busca el elemento que contiene el texto de error
            WebElement mensajeElemento = driver.findElement(By.xpath(CheckOutSelectores.mensajeErrorCompra));
            String mensajeActual = mensajeElemento.getText();

            // Se valida si el mensaje es igual al esperado
            assertEquals("El mensaje no coincide", mensajeEsperado, mensajeActual);
            System.out.println("El mensaje '" + mensajeEsperado + "' se ha mostrado correctamente.");
        } catch (org.openqa.selenium.NoSuchElementException e) {
            // Se alerta si no se encuentra el mensaje
            System.out.println("No se ha mostrado el mensaje '" + mensajeEsperado + "'.");
            throw new AssertionError("El mensaje no se ha mostrado correctamente: " + mensajeEsperado);
        }
    }

    //Metodo para validar que no se permite finalizar compra sin pais
    @Then("Validar mensaje de alerta {string}")
    public void validarMensajeDeAlerta(String mensajeEsperado) {
        WebDriver driver = miConfig.getDriver();

        try {
            // Se busca el elemento que contiene el texto de alerta
            WebElement mensajeElemento = driver.findElement(By.xpath(CheckOutSelectores.mensajeAlertaPais));
            String mensajeActual = mensajeElemento.getText();

            // Se valida si el mensaje es igual al esperado
            assertEquals("El mensaje no coincide", mensajeEsperado, mensajeActual);
            System.out.println("El mensaje '" + mensajeEsperado + "' se ha mostrado correctamente.");
        } catch (org.openqa.selenium.NoSuchElementException e) {
            System.out.println("No se ha mostrado el mensaje '" + mensajeEsperado + "'.");
            throw new AssertionError("El mensaje no se ha mostrado correctamente: " + mensajeEsperado);
        }
    }

    //Metodo para validar que no se permite finalizar compra sin aceptar terminos y condiciones
    @Then("Validar mensaje alerta {string}")
    public void validarMensajeAlerta(String mensajeEsperado) {
        WebDriver driver = miConfig.getDriver();

        try {
            // Se busca el elemento que contiene el texto de alerta
            WebElement mensajeElemento = driver.findElement(By.xpath(CheckOutSelectores.mensajeAlertaCondiciones));
            String mensajeActual = mensajeElemento.getText();

            // Se valida si el mensaje es igual al esperado
            assertEquals("El mensaje no coincide", mensajeEsperado, mensajeActual);
            System.out.println("El mensaje '" + mensajeEsperado + "' se ha mostrado correctamente.");
        } catch (org.openqa.selenium.NoSuchElementException e) {
            System.out.println("No se ha mostrado el mensaje '" + mensajeEsperado + "'.");
            throw new AssertionError("El mensaje no se ha mostrado correctamente: " + mensajeEsperado);
        }
    }


}
