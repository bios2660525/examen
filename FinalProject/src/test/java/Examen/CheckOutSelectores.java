package Examen;

public class CheckOutSelectores {

    static String botonPlaceOrder = "//div[@id='root']/div/div/div/div/button";

    static String comboPaises = "//div[@id='root']/div/div/div/div/div/select";

   // static String opcionAustralia = "//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[1]/select[1]";

    static String checkTermsConditions = "//div[@id='root']/div/div/div/div/input";

    static String botonProceed = "//div[@id='root']/div/div/div/div/button";

    static String mensajeCompraRealizada = "//div/div/div/div/div";

    static String mensajeErrorCompra = "//div[@id='cart']//span[contains(text(),'Error: Empty cart')]";

    static String mensajeAlertaPais = "//div[@id='cart']//span[contains(text(),'Error: No country select')]";

    static String mensajeAlertaCondiciones = "//div[@id='root']/div/div/div/div/span/b";
}
