package Examen;

import io.cucumber.core.options.Constants;
import org.junit.platform.suite.api.ConfigurationParameter;
import org.junit.platform.suite.api.IncludeEngines;
import org.junit.platform.suite.api.SelectClasspathResource;
import org.junit.platform.suite.api.Suite;

@Suite
@IncludeEngines("cucumber")

/* Si pongo "Examen/Equis.feature" puedo seleccionar uno en particular,
sino se ejecutan todos los features que esten en la carpeta */
@SelectClasspathResource("Examen")

@ConfigurationParameter(key = Constants.PLUGIN_PROPERTY_NAME, value = "pretty")
@ConfigurationParameter(key = Constants.GLUE_PROPERTY_NAME, value = "Examen")

/* Si quiero correr todas las pruebas, comentar la linea de abajo,
sino solo se corren las que esten dentro de la etiqueta */
//@ConfigurationParameter(key = Constants.FILTER_TAGS_PROPERTY_NAME, value = "@OrdenarNombre")

@ConfigurationParameter(key = Constants.PLUGIN_PROPERTY_NAME, value = "pretty, json:target/cucumber.json, html:reporte/cucumber-TotalPruebasreports.html")

public class ClaseRunner {
}
