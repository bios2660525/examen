package Examen;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Configuracion {

    protected static WebDriver driver;
    private static WebDriverWait wait2;
    protected static Actions action;

    static {
        ChromeOptions options = new ChromeOptions();
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        wait2 = new WebDriverWait(driver, Duration.ofSeconds(15));
        action = new Actions(driver);
    }

    public void clickElement(String locator) {
        if (findByXpath(locator).isDisplayed()) {
            findByXpath(locator).click();
        }
    }

    public boolean isElementNotPresent(By locator) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
            return true;
        } catch (NoSuchElementException e) {
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void escribir(String locator, String textToWrite) {
        findByXpath(locator).clear();
        findByXpath(locator).sendKeys(textToWrite);
    }

    public static WebElement findByXpath(String locator) {
        return wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
    }

    public String obtenerTexto(String locator) {
        return findByXpath(locator).getText();
    }

    public static WebDriver getDriver() {
        return driver;
    }


    public void accederPagina(String url) {
        driver.navigate().refresh();
        driver.get(url);
    }

    //@After
    public void quitNavegate() {
        driver.quit();
    }
}
