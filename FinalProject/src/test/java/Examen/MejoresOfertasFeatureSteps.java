package Examen;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;


public class MejoresOfertasFeatureSteps {

    Configuracion miConfig = new Configuracion();

    @Given("Ingresar a pagina {string}")
    public void ingresarPagina(String url) {
        miConfig.getDriver().get(url);
    }

    //Ingresa a la pantalla de mejores ofertas
    @Given("Clickea el boton {string}")
    public void clickearBoton(String boton) {
        WebDriver driver = miConfig.getDriver();
        WebElement botonTopDeals = driver.findElement(By.xpath(MejoresOfertasSelectores.botonOfertas));
        botonTopDeals.click();

        // Se cambia la navegacion a la ventana nueva que se abre
        for (String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle);
        }
    }

    //Se valida que se ingreso a la pantalla verificando que se encuentra el filtro de descuentos
    @Then("Validar que se ingreso a pantalla de ofertas")
    public void validarPantallaOfertas() {
        WebDriver driver = miConfig.getDriver();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement botonFiltro = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MejoresOfertasSelectores.filtroDescuento)));

        if (!botonFiltro.isDisplayed()) {
            throw new AssertionError("No se ingresó a la pantalla de ofertas.");
        }
    }

    /*Se crea un metodo que recorre un mapa de productos para validar
    que se encuentran presentes en la pantalla*/
    @Then("Validar que existen los siguientes productos:")
    public void validarProductosMostrados(List<String> productos) {
        WebDriver driver = miConfig.getDriver();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        // Mapa de productos
        Map<String, String> productosSelectores = Map.of(
                "Wheat", MejoresOfertasSelectores.textoWheat,
                "Tomato", MejoresOfertasSelectores.textoTomato,
                "Strawberry", MejoresOfertasSelectores.textoStrawberry,
                "Rice", MejoresOfertasSelectores.textoRice,
                "Potato", MejoresOfertasSelectores.textoPotato,
                "Pineapple", MejoresOfertasSelectores.textoPineapple,
                "Orange", MejoresOfertasSelectores.textoOrange,
                "Mango", MejoresOfertasSelectores.textoMango,
                "Guava", MejoresOfertasSelectores.textoGuava,
                "Dragon fruit", MejoresOfertasSelectores.textoDragonFruit
        );

        for (String producto : productos) {
            String selector = productosSelectores.get(producto);
            if (selector == null) {
                throw new AssertionError("No se encontró el selector para el producto: " + producto);
            }
            WebElement productoElemento = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selector)));
            if (!productoElemento.getText().contains(producto)) {
                throw new AssertionError("El producto " + producto + " no está presente en la página.");
            }
        }
    }

    @And("Seleccionar en desplegable cantidad a mostrar {string}")
    public void seleccionarDesplegableCantidad(String cantidad) {
        WebDriver driver = miConfig.getDriver();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement desplegable = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MejoresOfertasSelectores.desplegableMostrarCantidad)));
        Select select = new Select(desplegable);
        select.selectByVisibleText(cantidad);
    }


    @And("Escribir en la barra de busquda {string}")
    public void escribirEnLaBarraDeBusqueda(String texto) {
        WebDriver driver = miConfig.getDriver();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement barraBuscar = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MejoresOfertasSelectores.barraBuscar)));
        barraBuscar.sendKeys(texto);
    }

    @Then("Validar que se muestra el producto ingresado")
    public void validarProductoIngresado() {
        WebDriver driver = miConfig.getDriver();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement productoApple = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MejoresOfertasSelectores.productoApple)));
        if (!productoApple.getText().contains("Apple")) {
            throw new AssertionError("El producto 'Apple' no está presente en la página.");
        }
    }

    @And("Verificar primer producto de la lista")
    public void verificarPrimerProductoLista() {
        WebDriver driver = miConfig.getDriver();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        // Espera para que el producto sea visible
        WebElement primerProductoElemento = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MejoresOfertasSelectores.primerProdLista)));
        String primerProductoTexto = primerProductoElemento.getText().trim();
        System.out.println("El primer producto en la lista es: " + primerProductoTexto);
    }

    @When("Clickear en filtro {string}")
    public void clickearEnFiltro(String filtro) {
        WebDriver driver = miConfig.getDriver();
        WebElement filtroElemento = driver.findElement(By.xpath(MejoresOfertasSelectores.vegFruitName));
        filtroElemento.click();
    }

    @Then("Verificar que el primer producto de la lista es {string}")
    public void verificarPrimerProductoListaEs(String productoEsperado) {
        WebDriver driver = miConfig.getDriver();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        // Espera que el primer producto de la lista sea visible despues de aplicar orden por nombre
        WebElement primerProductoElemento = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MejoresOfertasSelectores.primerProdLista)));
        String primerProductoTexto = primerProductoElemento.getText().trim();

        // Verificar si el primer producto es el esperado
        assertEquals("El primer producto de la lista no es el esperado.", productoEsperado, primerProductoTexto);
        System.out.println("El primer producto de la lista despues de aplicar el filtro es: " + productoEsperado);
    }
}
