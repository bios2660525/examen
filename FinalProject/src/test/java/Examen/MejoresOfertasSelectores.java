package Examen;

public class MejoresOfertasSelectores {

    static String botonOfertas = "//div[@id='root']/div/header/div/div[3]/a[2]";

    static String filtroDescuento = "//div[@id='root']/div/div/div/div/div/div/table/thead/tr/th[3]";

    static String desplegableMostrarCantidad = "//div[@id='root']/div/div/div/div/div/div/div/div/div/select";

    static String textoWheat = "//div[@id='root']/div/div/div/div/div/div/table/tbody/tr/td";

    static String textoTomato = "//div[@id='root']/div/div/div/div/div/div/table/tbody/tr[2]/td";

    static String textoStrawberry = "//div[@id='root']/div/div/div/div/div/div/table/tbody/tr[3]/td";

    static String textoRice = "//div[@id='root']/div/div/div/div/div/div/table/tbody/tr[4]/td";

    static String textoPotato = "//div[@id='root']/div/div/div/div/div/div/table/tbody/tr[5]/td";

    static String textoPineapple = "//div[@id='root']/div/div/div/div/div/div/table/tbody/tr[6]/td";

    static String textoOrange = "//div[@id='root']/div/div/div/div/div/div/table/tbody/tr[7]/td";

    static String textoMango = "//div[@id='root']/div/div/div/div/div/div/table/tbody/tr[8]/td";

    static String textoGuava = "//div[@id='root']/div/div/div/div/div/div/table/tbody/tr[9]/td";

    static String textoDragonFruit = "//div[@id='root']/div/div/div/div/div/div/table/tbody/tr[10]/td";

    static String barraBuscar = "//div[@id='root']/div/div/div/div/div/div/div/div/div[2]/input";

    static String productoApple = "//div[@id='root']/div/div/div/div/div/div/table/tbody/tr[2]/td";

    static String primerProdLista = "//div[@id='root']/div/div/div/div/div/div/table/tbody/tr/td";

    static String vegFruitName = "//div[@id='root']/div/div/div/div/div/div/table/thead/tr/th/span[2]";

}
