package Examen;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PantallaPrincipalFeatureSteps {

    Configuracion miConfig = new Configuracion();

    @Given("Ingresar a la pag {string}")
    public void ingresarAPag(String url) {
        miConfig.getDriver().get(url);
    }

    @Given("Clickear agregar el producto {string}")
    public void clickearAgregarProducto(String producto) {
        WebDriver driver = miConfig.getDriver();
        WebElement addButton;

        switch (producto) {
            case "brocoli":
                addButton = driver.findElement(By.xpath(CarritoSelectores.addToCartBrocoli));
                break;
            case "zanahoria":
                addButton = driver.findElement(By.xpath(CarritoSelectores.addToCartZanahoria));
                break;
            default:
                throw new IllegalArgumentException("Producto no encontrado: " + producto);
        }
        addButton.click();
    }

    @When("Ingresar a otra pantalla {string}")
    public void ingresarAOtraPantalla(String pantalla) {
        WebDriver driver = miConfig.getDriver();
        WebElement botonOfertas = driver.findElement(By.xpath(MejoresOfertasSelectores.botonOfertas));
        botonOfertas.click();

        // Se cambia a la nueva pagina que se abre
        for (String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle);
        }
    }

    /*Se crea un metodo para volver a la pantalla principal usando la url
    ya que no se encuentran selectores disponibles para esta accion*/

    @When("Volver a la pantalla principal")
    public void volverAPantallaPrincipal() {
        WebDriver driver = miConfig.getDriver();
        // Navegar de regreso a la url principal
        driver.get("https://rahulshettyacademy.com/seleniumPractise/#/");
    }


    @When("Clickear boton de carrito {string}")
    public void clickearBotonDeCarrito(String carrito) {
        WebDriver driver = miConfig.getDriver();
        WebElement carritoButton = driver.findElement(By.xpath(CarritoSelectores.carrito));
        carritoButton.click();
    }

    //Se valida si el producto se encuentra en el carrito despues de navegar por la web
    @Then("Validar que se encuentre el producto {string}")
    public void validarProductoEnCarrito(String producto) {
        WebDriver driver = miConfig.getDriver();
        WebElement productoEnCarrito;

        try {
            productoEnCarrito = driver.findElement(By.xpath(CarritoSelectores.brocoliCarrito));
            String productoActual = productoEnCarrito.getText();
            assertTrue("El producto no se encuentra en el carrito", productoActual.toLowerCase().contains(producto.toLowerCase()));
        } catch (org.openqa.selenium.NoSuchElementException e) {
            throw new AssertionError("El producto no se encuentra en el carrito: " + producto);
        }
    }

    //Se valida el precio inicial que se muestra en la web
    @Given("Verificar que el precio inicial es {string}")
    public void verificarPrecioInicial(String precioEsperado) {
        WebDriver driver = miConfig.getDriver();
        WebElement precioElemento = driver.findElement(By.xpath(PantallaPrincipalSelectores.tagPrecio));
        String precioActual = precioElemento.getText().trim();
        assertTrue("El precio inicial no contiene el valor esperado", precioActual.contains(precioEsperado));
    }

    //Se valida que el precio se actualizo al indicado en el feature
    @Then("Validar que se actualizo el precio a {string}")
    public void validarPrecioActualizado(String precioEsperado) {
        WebDriverWait wait = new WebDriverWait(miConfig.getDriver(), Duration.ofSeconds(10));
        WebElement precioSelector = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PantallaPrincipalSelectores.tagPrecio)));

        // Se obtiene el precio del selector
        String precioActual = precioSelector.getText();

        // Se verifica si el precio coincide con el esperado
        if (!precioActual.contains(precioEsperado)) {
            throw new AssertionError("El precio actualizado no contiene el valor esperado. Precio actual: " + precioActual + ", Precio esperado: " + precioEsperado);
        }
    }


    //Se verifica que la cantidad de items inicial es 0
    @Given("Verificar que la cantidad inicial es {string}")
    public void verificarCantidadInicial(String cantidadEsperada) {
        WebDriver driver = miConfig.getDriver();
        WebElement cantidadElemento = driver.findElement(By.xpath(PantallaPrincipalSelectores.tagItems));
        String cantidadTexto = cantidadElemento.getText();
        if (!cantidadTexto.contains(cantidadEsperada)) {
            throw new AssertionError("La cantidad inicial no es correcta. Resultado esperado: " + cantidadEsperada + ", Resultado actual: " + cantidadTexto);
        }
        System.out.println("La cantidad inicial es correcta: " + cantidadEsperada);
    }

    //Se verifica que se actualizo la cantidad de items ingresado en el feature
    @Then("Validar que se actualizo la cantidad a {string}")
    public void validarCantidadActualizada(String cantidadEsperada) {
        WebDriver driver = miConfig.getDriver();
        WebElement cantidadElemento = driver.findElement(By.xpath(PantallaPrincipalSelectores.tagItems));
        String cantidadTexto = cantidadElemento.getText();
        if (!cantidadTexto.contains(cantidadEsperada)) {
            throw new AssertionError("La cantidad no se actualizo correctamente. Resultado esperado: " + cantidadEsperada + ", Resultado actual: " + cantidadTexto);
        }
        System.out.println("La cantidad se actualizo correctamente a: " + cantidadEsperada);
    }


}
