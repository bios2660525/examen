Feature: Busqueda

  Background:
    Given Acceder a la pagina "https://rahulshettyacademy.com/seleniumPractise/#/"

  @BusquedaExitosa
  Scenario: Buscar un producto valido
    Given Seleccionar barra de busqueda "Barra busqueda"
    When Escribir producto "Apple"
    Then Validar producto "Apple"

  @BusquedaInvalida
  Scenario: Buscar un producto invalido
    Given Seleccionar barra de busqueda "Barra de busqueda"
    When Escribir producto "aa"
    Then Validar mensaje "Sorry, no products matched your search!"


