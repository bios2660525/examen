Feature: Carrito

  Background:
    Given Acceder a la pag "https://rahulshettyacademy.com/seleniumPractise/#/"

  @CarritoVacio
  Scenario: Verificar carrito vacio
    Given Clickear el boton carrito "Carrito"
    Then Validar el mensaje "You cart is empty!"

  @AgregarProducto
  Scenario: Agregar producto al carrito
    Given Clickear agregar producto "brocoli"
    When Clickear el boton carrito "Carrito"
    Then Validar existencia producto "Brocoli"

  @EliminarProducto
  Scenario: Eliminar producto del carrito
    Given Clickear agregar producto "Zanahoria"
    When Clickear el boton carrito "Carrito"
    And Clickear boton eliminar "Eliminar"
    Then Validar el mensaje "You cart is empty!"

  @CantidadCero
  Scenario: No permitir agregar cantidad de producto 0 al carrito
    Given Escribir cantidad de producto "Tomate" "0"
    When Clickear agregar producto "Tomate"
    And Clickear el boton carrito "Carrito"
    Then Validar el mensaje no se puede continuar "Error: Check your cart"

  @CodigoPromoInvalido
  Scenario: Alertar por codigo de promo invalido
    Given Clickear agregar producto "Brocoli"
    And Clickear el boton carrito "Carrito"
    And Clickear el boton proceder al checkout "Checkout"
    When Escribir codigo de promo "123"
    And Clickear el boton aplicar promo "Aplicar"
    Then Validar el mensaje de promo "Invalid code ..!"


