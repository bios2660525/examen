Feature: CheckOut

  Background:
    Given Ingresar a la pagina "https://rahulshettyacademy.com/seleniumPractise/#/"

  @CheckOut
  Scenario: Realizar checkout exitoso
    Given Clickear agregar un producto "brocoli"
    When Ingresar al carrito "Carrito"
    And Clickear boton checkout "Proceed to check out"
    And Clickear boton de pedido "Place order"
    And Clickear combobox "Paises"
    And Seleccionar pais "Australia"
    And Clickear checkbox de condiciones "Agree to the Terms & Conditions"
    And Clickear boton para finalizar "Proceed"
    Then Validar mensaje de confirmacion "Thank you, your order has been placed successfully You'll be redirected to Home page shortly!!"

  @CheckOutVacio
  Scenario: No permitir checkout con carrito vacio
    Given Escribir la cantidad de producto "tomate" "0"
    When Clickear agregar un producto "tomate"
    And Ingresar al carrito "Carrito"
    And Clickear boton checkout "Proceed to check out"
    And Clickear boton de pedido "Place order"
    And Clickear combobox "Paises"
    And Seleccionar pais "Australia"
    And Clickear checkbox de condiciones "Agree to the Terms & Conditions"
    And Clickear boton para finalizar "Proceed"
    Then Validar el mensaje de error "Error: Empty cart"

    @CheckOutSinPais
    Scenario: No permitir checkout sin seleccionar un pais
      Given Clickear agregar un producto "brocoli"
      When Ingresar al carrito "Carrito"
      And Clickear boton checkout "Proceed to check out"
      And Clickear boton de pedido "Place order"
      And Clickear checkbox de condiciones "Agree to the Terms & Conditions"
      And Clickear boton para finalizar "Proceed"
      Then Validar mensaje de alerta "Error: No country select"

      @CheckOutSinCondiciones
      Scenario: No permitir checkout sin aceptar terminos y condiciones
        Given Clickear agregar un producto "brocoli"
        When Ingresar al carrito "Carrito"
        And Clickear boton checkout "Proceed to check out"
        And Clickear boton de pedido "Place order"
        And Clickear combobox "Paises"
        And Seleccionar pais "Australia"
        And Clickear boton para finalizar "Proceed"
        Then Validar mensaje alerta "Please accept Terms & Conditions - Required"