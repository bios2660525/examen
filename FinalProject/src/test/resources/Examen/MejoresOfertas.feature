Feature: MejoresOfertas

  Background:
    Given Ingresar a pagina "https://rahulshettyacademy.com/seleniumPractise/#/"

  @AccederOfertas
  Scenario: Acceder a las mejores ofertas exitosamente
    Given Clickea el boton "Top Deals"
    Then Validar que se ingreso a pantalla de ofertas

  @Mostrar5Productos
  Scenario: Se muestran 5 productos en las ofertas
    Given Clickea el boton "Top Deals"
    Then Validar que existen los siguientes productos:
      | Wheat      |
      | Tomato     |
      | Strawberry |
      | Rice       |
      | Potato     |

  @Mostrar10Productos
  Scenario: Se muestran 10 productos en las ofertas
    Given Clickea el boton "Top Deals"
    And Seleccionar en desplegable cantidad a mostrar "10"
    Then Validar que existen los siguientes productos:
      | Wheat        |
      | Tomato       |
      | Strawberry   |
      | Rice         |
      | Potato       |
      | Pineapple    |
      | Orange       |
      | Mango        |
      | Guava        |
      | Dragon fruit |

  @BuscarOferta
  Scenario: Buscar un producto en las ofertas
    Given Clickea el boton "Top Deals"
    And Escribir en la barra de busquda "apple"
    Then Validar que se muestra el producto ingresado

  @OrdenarNombre
  Scenario: Ordenar ofertas por nombre
    Given Clickea el boton "Top Deals"
    And Verificar primer producto de la lista
    When Clickear en filtro "veg/fruit name"
    Then Verificar que el primer producto de la lista es "Almond"
