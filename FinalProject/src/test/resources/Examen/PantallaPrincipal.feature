Feature: PantallaPrincipal

  Background:
    Given Ingresar a la pag "https://rahulshettyacademy.com/seleniumPractise/#/"

  @PermanenciaCarrito
  Scenario: Mantener la informacion del carrito al navegar en la web
    Given Clickear agregar el producto "brocoli"
    When Ingresar a otra pantalla "Top Deals"
    And Volver a la pantalla principal
    And Clickear boton de carrito "Carrito"
    Then Validar que se encuentre el producto "brocoli"

  @PrecioTotal
  Scenario: Se actualiza el precio total a mostrar
    Given Verificar que el precio inicial es "0"
    When Clickear agregar el producto "brocoli"
    Then Validar que se actualizo el precio a "120"

    @CantidadItems
    Scenario: Se actualiza la cantidad de items que estan en el carrito
      Given Verificar que la cantidad inicial es "0"
      When Clickear agregar el producto "zanahoria"
      And Clickear agregar el producto "brocoli"
      Then Validar que se actualizo la cantidad a "2"

