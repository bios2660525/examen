package Examen;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;

import static Examen.Configuration.driver;

public class LoginFeatureSteps {

    Configuration miConfig = new Configuration();
    RegisterLocators registerLocators = new RegisterLocators();

    @Given("Dar click en boton {string}")
    public void darClickEnBoton(String boton) {
        switch (boton) {
            case "Sign In":
                miConfig.clickElement(RegisterLocators.buttonSignIn);
                break;
            default:
                throw new IllegalArgumentException("Boton desconocido: " + boton);
        }
    }

    @Given("Ingresar usuario {string}")
    public void ingresarUsuario(String usuario) {
        miConfig.escribir(RegisterLocators.userNameField, usuario);
    }

    @Given("Ingresar contrasena {string}")
    public void ingresarContrasena(String contrasena) {
        miConfig.escribir(RegisterLocators.passwordField, contrasena);
    }

    @When("Dar click en boton {string}")
    public void darClickEnBotonLogin(String boton) {
        switch (boton) {
            case "Login":
                miConfig.clickElement(RegisterLocators.buttonLogin);
                break;
            default:
                throw new IllegalArgumentException("Boton desconocido: " + boton);
        }
    }

    @Then("Validar ingreso de usuario")
    public void validarIngresoDeUsuario() {
        String expectedUserName = "pepitouno3";
        miConfig.clickElement(RegisterLocators.buttonMyAccount);
        String locator = RegisterLocators.userNameText.replace("{username}", expectedUserName);
        String actualText = miConfig.obtenerTexto(locator);
        if (!actualText.contains(expectedUserName)) {
            throw new AssertionError("El nombre de usuario esperado no se encontró. Esperado: " + expectedUserName + ", pero se obtuvo: " + actualText);
        }
    }

}
