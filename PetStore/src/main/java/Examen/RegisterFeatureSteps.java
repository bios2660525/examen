package Examen;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.datatable.DataTable;

import static Examen.Configuration.driver;

public class RegisterFeatureSteps {

    //Se crean los objetos para acceder a los localizadores
    Configuration miConfig = new Configuration();
    RegisterLocators registerLocators = new RegisterLocators();

    //Se crea un switch para dar click en el boton correspondiente segun el valor del boton
    @Given("Dar click en boton {string}")
    public void darClickEnBoton(String boton) {
        switch (boton) {
            case "Sign In":
                miConfig.clickElement(RegisterLocators.buttonSignIn);
                break;
            case "Register Now":
                miConfig.clickElement(RegisterLocators.buttonRegisterNow);
                break;
            case "Save Account Information":
                miConfig.clickElement(RegisterLocators.saveAccountInfo);
                break;
            case "My Account":
                miConfig.clickElement(RegisterLocators.buttonMyAccount);
                break;
            default:
                throw new IllegalArgumentException("Boton desconocido: " + boton);
        }
    }
//Se completa el formulario de registro segun los valores que se extraen de la tabla del feature
    @And("Completar formulario")
    public void completarFormulario(DataTable dataTable) {
        java.util.List<java.util.Map<String, String>> rows = dataTable.asMaps(String.class, String.class);
        for (java.util.Map<String, String> columns : rows) {
            miConfig.escribir(RegisterLocators.userId, columns.get("User ID"));
            miConfig.escribir(RegisterLocators.password, columns.get("New Password"));
            miConfig.escribir(RegisterLocators.password2, columns.get("Repeat Password"));
            miConfig.escribir(RegisterLocators.firstN, columns.get("First name"));
            miConfig.escribir(RegisterLocators.lastN, columns.get("Last name"));
            miConfig.escribir(RegisterLocators.email, columns.get("Email"));
            miConfig.escribir(RegisterLocators.phone, columns.get("Phone"));
            miConfig.escribir(RegisterLocators.address1, columns.get("Address1"));
            miConfig.escribir(RegisterLocators.address2, columns.get("Address2"));
            miConfig.escribir(RegisterLocators.city, columns.get("City"));
            miConfig.escribir(RegisterLocators.state, columns.get("State"));
            miConfig.escribir(RegisterLocators.zip, columns.get("Zip"));
            miConfig.escribir(RegisterLocators.country, columns.get("Country"));
        }
    }

    //Se valida el ingreso accediendo al boton my account y comparando si el nombre de usuario obtenido es igual al que se esperaba
    @Then("Validar ingreso de usuario")
    public void validarIngresoDeUsuario() {
        String expectedUserName = "Fulanit01";
        miConfig.clickElement(RegisterLocators.buttonMyAccount);
        String locator = RegisterLocators.userNameText.replace("{username}", expectedUserName);
        String actualText = miConfig.obtenerTexto(locator);
        if (!actualText.contains(expectedUserName)) {
            throw new AssertionError("Expected user name not found. Expected: " + expectedUserName + ", but got: " + actualText);
        }
    }
}
