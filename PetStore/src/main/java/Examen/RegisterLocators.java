package Examen;

public class RegisterLocators {

    static String buttonSignIn = "//a[contains(text(),'Sign In')]";

    static String buttonRegisterNow = "//a[contains(text(),'RegisterFeatureSteps Now!')]";

    static String userNameField = "//input[@name='username']";

    static String passwordField = "//input[@name='password']";

    static String buttonLogin = "//div[@id='Catalog']/form/input";

    static String buttonMyAccount = "//a[contains(text(),'My Account')]";

    static String userNameText = "//td[contains(.,'{username}')]";

    static String userId = "//div[@id='Catalog']/form/table/tbody/tr/td[2]/input";
    static String password = "//input[@name='password']";
    static String password2 = "//input[@name='repeatedPassword']";
    static String firstN = "//input[@name='account.firstName']";
    static String lastN = "//input[@name='account.lastName']";
    static String email = "//input[@name='account.email']";
    static String phone = "//input[@name='account.phone']";
    static String address1 = "//input[@name='account.address1']";
    static String address2 = "//input[@name='account.address2']";
    static String city = "//div[@id='Catalog']/form/table[2]/tbody/tr[7]/td[2]/input";
    static String state = "//input[@name='account.state']";
    static String zip = "//input[@name='account.zip']";
    static String country = "//input[@name='account.country']";
    static String saveAccountInfo = "//input[@name='newAccount']";

}
