package Examen;

public class ShoppingCartLocators {

    //Localizadores para búsqueda de producto

   static String buttonCart = "//div[@id='Catalog']form/input";

   static String searchButton = "//input[@name='searchProducts']";

   static String searchBar = "//input[@name='keyword']";

static String buttonDog = "//div[@id='SidebarContent']/a[2]/img";

static String productId = "//a[contains(text(),'K9-BD-01')]";

static String buttonAddCart = "//a[contains(text(),'Add to Cart')]";

static String buttonProceedToCheckOut = "//a[contains(text(),'Proceed to Checkout')]";

static String buttonContinue = "//input[@name='newOrder']";

static String buttonConfirm = "//div[@id='Catalog']/a";

static String messageConfirm = "//li[contains(.,'Thank you, your order has been submitted.')]";

}
