package Examen;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;

public class ShoppingFeatureSteps {

    Configuration miConfig = new Configuration();
    RegisterLocators registerLocators = new RegisterLocators();
    ShoppingCartLocators shoppingCartLocators = new ShoppingCartLocators();

    @Given("Dar click en boton {string}")
    public void darClickEnBoton(String boton) {
        switch (boton) {
            case "Sign In":
                miConfig.clickElement(RegisterLocators.buttonSignIn);
                break;
            case "Cart":
                miConfig.clickElement(ShoppingCartLocators.buttonCart);
                break;
            case "Login":
                miConfig.clickElement(RegisterLocators.buttonLogin);
                break;
            case "search":
                miConfig.clickElement(ShoppingCartLocators.searchButton);
                break;
            case "dog":
                miConfig.clickElement(ShoppingCartLocators.buttonDog);
                break;
            case "ProductId":
                miConfig.clickElement(ShoppingCartLocators.productId);
                break;
            case "AddCart":
                miConfig.clickElement(ShoppingCartLocators.buttonAddCart);
                break;
            case "ProceedToCheckOut":
                miConfig.clickElement(ShoppingCartLocators.buttonProceedToCheckOut);
                break;
            case "Continue":
                miConfig.clickElement(ShoppingCartLocators.buttonContinue);
                break;
            case "Confirma":
                miConfig.clickElement(ShoppingCartLocators.buttonConfirm);
                break;
            default:
                throw new IllegalArgumentException("Boton desconocido: " + boton);
        }
    }

    @Given("Ingresar usuario {string}")
    public void ingresarUsuario(String usuario) {
        miConfig.escribir(RegisterLocators.userNameField, usuario);
    }

    @Given("Ingresar contrasena {string}")
    public void ingresarContrasena(String contrasena) {
        miConfig.escribir(RegisterLocators.passwordField, contrasena);
    }

    @And("Dar click en {string}")
    public void darClickEnElemento(String elemento) {
        switch (elemento) {
            case "search bar":
                miConfig.clickElement(ShoppingCartLocators.searchBar);
                break;
            default:
                throw new IllegalArgumentException("Elemento desconocido: " + elemento);
        }
    }

    @And("Escribir producto {string}")
    public void escribirProducto(String producto) {
        miConfig.escribir(ShoppingCartLocators.searchBar, producto);
    }

    @Then("Confirmar mensaje {string}")
    public void confirmarMensaje(String mensaje) {
        String actualText = miConfig.obtenerTexto(ShoppingCartLocators.messageConfirm);
        if (!actualText.contains(mensaje)) {
            throw new AssertionError("Expected message not found. Expected: " + mensaje + ", but got: " + actualText);
        }
    }
}
