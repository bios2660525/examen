Feature: Login

  Background:
    Given Acceder a la pag "https://petstore.octoperf.com/actions/Catalog.action"

  @Login
  Scenario: Iniciar sesion
    Given Dar click en boton "Sign In"
    And Ingresar usuario "pepitouno3"
    And Ingresar contrasena "123456"
    When Dar click en boton "Login"
    Then Validar ingreso de usuario

  Scenario: Iniciar sesion con usuario inexistente
    Given Dar click en boton "Sign In"
    And Ingresar usuario "Inexistente"
    And Ingresar contrasena "123456"
    When Dar click en boton "Login"
    Then Validar ingreso fallido de usuario

  Scenario: Iniciar sesion con contraseña invalida para el usuario existente
    Given Dar click en boton "Sign In"
    And Ingresar usuario "pepitouno3"
    And Ingresar contrasena "1234"
    When Dar click en boton "Login"
    Then Validar ingreso de usuario