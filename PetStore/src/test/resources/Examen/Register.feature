Feature: Register

  Background:
    Given Acceder a la pag "https://petstore.octoperf.com/actions/Catalog.action"

  @Register
  Scenario: Registrar usuario
    Given Dar click en boton "Sign In"
    And Dar click en boton "Register Now"
    And Completar formulario
      | User ID   | New Password | Repeat Password | First name | Last name | Email             | Phone | Address1 | Address2 | City | State | Zip | Country |
      | Fulanit01 | 123456       | 123456          | Fulano     | Apellido  | fulanit0@mail.com | 111   | aaaa     | bbbb     | ccc  | ddd   | 111 | Uruguay |
    When Dar click en boton "Save Account Information"
    Then Validar ingreso de usuario



  Scenario: Intentar registrar usuario existente
    Given Dar click en boton "Sign In"
    And Dar click en boton "Register Now"
    And Completar formulario
      | User ID   | New Password | Repeat Password | First name | Last name | Email             | Phone | Address1 | Address2 | City | State | Zip | Country |
      | Fulanito1 | 123456       | 123456          | Fulano     | Apellido  | fulanito@mail.com | 111   | aaaa     | bbbb     | ccc  | ddd   | 111 | Uruguay |
    When Dar click en boton "Save Account Information"


  Scenario: Intentar registrar usuario con contraseña invalida
    Given Dar click en boton "Sign In"
    And Dar click en boton "Register Now"
    And Completar formulario
      | User ID   | New Password | Repeat Password | First name | Last name | Email             | Phone | Address1 | Address2 | City | State | Zip | Country |
      | Fulanito1 | 1            | 1               | Fulano     | Apellido  | fulanito@mail.com | 111   | aaaa     | bbbb     | ccc  | ddd   | 111 | Uruguay |
    When Dar click en boton "Save Account Information"
