Feature: Shopping and search

  Background:
    Given  Acceder a la pag "https://petstore.octoperf.com/actions/Catalog.action"

  @Search
  Scenario: Buscar un producto
    Given Dar click en boton "Sign In"
    And Ingresar usuario "pepitouno3"
    And Ingresar contrasena "123456"
    And Dar click en boton "Login"
    And Dar click en "search bar"
    And Escribir producto "dog"
    When Dar click en botón "search"

  @ShoppingCart
  Scenario: Ver carrito de compras
    Given Dar click en boton "Sign In"
    And Ingresar usuario "pepitouno3"
    And Ingresar contrasena "123456"
    When Dar click en boton "Login"
    And Dar click en boton "Cart"

  @CompletePurchase
  Scenario: Completar una compra de un producto
    Given Dar click en boton "Sign In"
    And Ingresar usuario "pepitouno3"
    And Ingresar contrasena "123456"
    And Dar click en boton "Login"
    When Dar click en boton "dog"
    And Dar click en boton "ProductId"
    And Dar click en boton "AddCart"
    And Dar click en boton "ProceedToCheckOut"
    And Dar click en boton "Continue"
    And Dar click en boton "Confirma"
    Then Confirmar mensaje "Thank you, your order has been submitted"